# 분류 기법: 의사 결정 트리와 포레스트

## <a name="intro"></a> 들어가며
이 포스팅에서는 분류 작업에 의사결정 트리와 포레스트를 사용하는 방법을 설명할 것이다. 분류는 지도학습의 한 유형으로, 레이블이 지정된 데이터 집합을 가지고 있으며 일부 특징을 기반으로 새로운 데이터의 레이블을 예측하고자 한다. 예를 들어 이메일을 내용을 기반으로 스팸 여부를 분류하거나 고객의 구매 이력을 기반으로 충성도와 비 충성도로 분류할 수 있다.

의사결정 트리와 포레스트는 수치적 특징과 범주적 특징을 모두 다룰 수 있는 인기 있고 강력한 기계학습 기법이며, 결측값과 이상치도 다룰 수 있다. 또한 예측을 하기 위한 명확한 규칙 집합을 제공하기 때문에 해석과 시각화가 용이하다.

이 포스팅을 모두 이해한다면 다음 작업을 수행할 수 있다.

- 의사결정 트리와 숲의 기본 개념과 용어 이해
- Python과 scikit-Learn을 사용하여 의사결정 트리 분류기 구축 및 평가
- Python과 scikit-Learn을 사용하여 의사결정 포레스트 분류기 구축 및 평가
- 의사결정 트리와 포레스트의 장단점 비교
- 실제 시나리오에서 의사결정 트리와 포레스트의 어플리케이션과 예를 살펴본다

시작하기 전에 컴퓨터에 Python을 설치하고 다음 라이브러리를 설치해야 한다.

```
# Install the required libraries
pip install numpy pandas scikit-learn matplotlib
```

또한 이 포스팅에 사용할 데이터세트도 다운로드해야 한다. 유명한 아이리스 데이터 세트를 변형한 것으로, setosa, versicolor와 virginica 세 종의 홍채 꽃 샘플 150개를 포함하고 있다. 각 샘플에는 센티미터 단위로 측정된 세팔 길이, 세팔 폭, 꽃잎 길이, 꽃잎 폭의 네 가지 특징(feature)이 있다. 데이터세트에는 샘플이 세토사 종에서 왔는지 아닌지를 나타내는 이진 레이블인 다섯 번째 특징도 있다. 데이터 세트는 [여기](https://www.kaggle.com/datasets/dolphin123456789/iris-csv)에서 다운로드할 수 있다.

이제 모든 것을 준비했으니, 시작하자!

## <a name="sec_02"></a> 의사결정 트리란?
의사결정 트리는 일련의 의사결정과 가능한 결과를 그래픽으로 표현한 것이다. 노드와 가지로 구성되어 있는데, 각 노드는 특징에 대한 시험이나 질문을 나타내고, 각 가지는 결과나 답을 나타낸다. 각 노드는 가지로 연결되어 트리와 같은 구조를 이룬다. 맨 위의 노드를 루트 노드라고 하고, 맨 아래의 노드를 리프 노드라고 한다. 리프 노드는 최종 예측 또는 클래스 레이블을 나타낸다.

예를 들어, 여러분이 동물의 특징을 기반으로 분류하려고 한다고 가정해자. 여러분은 의사결정 트리를 사용하여 다음과 같은 질문을 할 수 있다. 털이 있나요? 날개가 있나요? 알을 낳나요? 대답에 따라, 여러분은 동물의 가능한 클래스을 좁힐 수 있다. 다음은 이 일을 위한 간단한 의사결정 트리이다.

```python
# This is a pseudo-code representation of the decision tree
if animal has fur:
    if animal has wings:
        animal is a bat
    else:
        if animal lays eggs:
            animal is a platypus
        else:
            animal is a mammal
else:
    if animal has wings:
        if animal lays eggs:
            animal is a bird
        else:
            animal is an insect
    else:
        animal is a reptile
```

보다시피, 의사결정 트리는 일련의 규칙을 기반으로 데이터를 분류하는 간단하고 직관적인 방법이다. 그러나 데이터로부터 이러한 규칙을 어떻게 만들 수 있을까? 각 노드에서 어떤 특징을 테스트할지 어떻게 결정할 수 있을까? 언제 트리 분할을 중단해야 하는지 어떻게 알 수 있을까? 이것은 [다음 절](#sec_03)에서 대답할 질문이다.

## <a name="sec_03"></a> 의사결정 트리를 구축하는 방법
데이터세트에서 의사결정 트리를 구축하기 위해서는 분할과 중지의 두 가지 주요 단계로 구성된 일반적인 알고리즘을 따라야 한다. 분할은 특징과 임계값을 기반으로 데이터를 더 작은 부분집합으로 나누는 과정이다. 중지는 분할을 언제 중단하고 각 리프 노드에 클래스 레이블을 할당할지 결정하는 과정이다. 이 단계들이 어떻게 작동하는지 좀 더 자세히 살펴보자.

**분할**
분할의 목표는 가능한 한 순수한 노드를 만드는 것인데, 이는 노드가 한 클래스 또는 한 클래스에 다수의 샘플들을 포함한다는 것을 의미한다. 이를 위해서는 각 노드에서 데이터를 분할할 수 있는 최적의 특징과 최적의 임계값을 찾아야 한다. 가장 좋은 특징은 분할이 노드의 불순물을 얼마나 감소시키는지를 나타내는 최대 정보 이득을 제공하는 특징이다. 가장 좋은 임계값은 선택한 특징에 대한 정보 이득을 극대화하는 것이다.

노드의 불순물을 측정하는 방법에는 엔트로피, 지니 지수, 오분류율 등 여러 가지가 있다. 엔트로피는 노드에 얼마나 많은 불확실성이 있는지를 측정하는 것으로 다음과 같이 계산된다.

```python
# Entropy formula
entropy = -sum(p * log2(p)) for all classes
```

여기서 `p`는 노드에서 각 클래스의 샘플이 차지하는 비율이다. 노드가 순수할 때 엔트로피는 0이며, 이는 노드가 한 클래스의 샘플만 포함한다는 것을 의미한다. 엔트로피는 노드가 균형을 이룰 때 최대이며, 이는 각 클래스의 샘플 수가 같다는 것을 의미한다.

지니 지수는 불순물의 또 다른 척도이며 다음과 같이 계산된다.

```python
# Gini index formula
gini = 1 - sum(p ** 2) for all classes
```

여기서 `p`는 앞과 같다. 지니 지수도 노드가 순수할 때는 0이 되고, 노드가 균형을 이룰 때는 최대가 된다.

오분류율은 불순물을 측정하는 가장 간단한 방법으로 다음과 같이 계산된다.

```python
 # Misclassification rate formula
misclassification = 1 - max(p) for all classes
```

여기서 `p`는 앞과 같다. 노드가 순수할 때 오분류율은 0이고, 노드가 균형을 이룰 때는 최소이다.

정보 이득은 부모 노드의 불순물과 자식 노드의 불순물의 가중 평균의 차이이다. 이는 다음과 같이 계산된다.

```python
# Information gain formula
information_gain = impurity(parent) - (weight(left) * impurity(left) + weight(right) * impurity(right))
```

여기서 불순물은 위에서 언급한 측정값 중 하나이며, 가중치는 각 자식 노드에 대한 표본의 비율이다. 정보 이득은 자식 노드들이 순수할 때 최대가 되고, 자식 노드들이 균형을 이룰 때 최소가 된다.

데이터를 분할할 수 있는 최선의 특징과 최선의 임계값을 찾기 위해서는 가능한 모든 특징과 가능한 모든 임계값을 반복하고 각 조합에 대한 정보 이득을 계산해야 한다. 가장 높은 정보 이득을 주는 조합이 최선의 조합이다. 이 과정은 중지 기준이 충족될 때까지 각 자식 노드에 대해 재귀적으로 반복된다.

**중지(stopping)**
중지의 목표는 과적합을 방지하는 것인데, 이때 의사결정 트리가 너무 복잡해져서 노이즈와 훈련 데이터의 세부 정보를 학습하여 일반화가 잘 되지 않고 분산이 큰 경우이다. 과적합을 피하기 위해서는 트리가 어느 정도의 복잡성이나 순도에 도달하면 분할을 중지해야 한다. 중지 기준을 정의하는 방법은 다음과 같다.

- 트리에 대한 최대 깊이 설정, 즉 트리가 가질 수 있는 레벨 또는 분할 수.
- 분할할 노드에 대한 최소 샘플 수 설정, 즉 분할을 위해 노드가 고려해야 하는 최소 샘플 수.
- 분할할 노드에 대한 최소 정보 이득, 즉 노드가 분할되어야 하는 최소 정보 이득을 설정하는 것이다.
- 리프 노드가 가질 수 있는 최소 불순물 양인 리프 노드에 대한 최소 불순물 설정.

일단 정지 기준이 충족되면, 노드는 더 이상 분할되지 않고 리프 노드가 된다. 리프 노드의 클래스 레이블은 노드 내 샘플들의 다수결에 의해, 또는 노드 내 클래스들의 확률 분포에 의해 결정된다.

의사결정 트리를 구축하기 위한 일반적인 알고리즘을 살펴보았으므로 scikit-learn 라이브러리를 사용하여 Python에서 어떻게 구현할 수 있는지 알아보자.

<!--
구현 없음
-->

## <a name="sec_04"></a> 의사결정 포레스트란?
의사결정 포레스트는 데이터 및/또는 특징의 상이한 부분집합에 대해 훈련되는 의사결정 트리들의 집합이다. 의사결정 포레스트의 아이디어는 분류기의 정확성과 강건성을 향상시키기 위해 다수의 의사결정 트리들의 예측들을 결합하는 것이다. 이 기법은 앙상블 학습이라고도 하며, 약한 학습자들의 그룹이 그들의 산출물을 투표하거나 평균화함으로써 강한 학습자를 형성할 수 있다는 원리에 기초한다.

의사결정 포리스트를 만드는 데에는 배깅, 부스팅 또는 랜덤 포리스트와 같은 다양한 방법이 있다. 배깅은 데이터의 랜덤 샘플에 대해 각 의사결정 트리를 대체하여 훈련시키는 방법으로, 일부 샘플이 동일한 샘플에서 반복될 수 있음을 의미한다. 부스팅은 데이터의 가중치 샘플에 대해 각 의사결정 트리를 훈련시키는 방법으로, 이전 트리의 오류를 기반으로 가중치가 조정된다. 랜덤 포리스트는 배깅의 특별한 경우로, 각 의사결정 트리가 모든 특징을 사용하는 것이 아니라 특징의 랜덤 부분집합에 대해 훈련된다.

의사결정 포레스트의 가장 큰 장점은 개별 트리의 잡음과 오류를 평균화함으로써 의사결정 트리의 분산과 과적합을 줄일 수 있다는 것이다. 또한 그들은 크고 복잡한 데이터세트를 처리할 수 있으며, 특징이 분할에 얼마나 자주 사용되는지에 기초하여 특징 중요도의 척도를 제공할 수 있다. 의사결정 포레스트의 가장 큰 단점은 여러 트리를 훈련하고 저장해야 하기 때문에 계산 비용과 메모리 사용량을 증가시킬 수 있다는 것이다. 또한 그들은 예측을 하기 위한 단일하고 명확한 규칙 집합을 제공하지 않기 때문에 해석하고 시각화하기가 더 어려울 수 있다.

[다음 절](#sec_05)에서는 Python과 scikit-learn을 이용하여 의사결정 포레스트 분류기를 구축하고 평가하는 방법을 살펴볼 것이다.

## <a name="sec_05"></a> 의사결정 포레스트를 구축하는 방법
의사결정 포레스트는 의사결정 트리들의 집합으로, 데이터의 서로 다른 부분집합에 대해 훈련된 후 결합하여 최종 예측을 하는 것이다. 이 기법의 배경은 여러 가지 트리들의 예측을 평균화함으로써 하나의 의사결정 트리의 분산과 과적합을 줄이는 것이다. 의사결정 포레스트는 트리를 구축하는 과정에 무작위성을 도입하여 트리의 다양성을 증가시키기 때문에 랜덤 포레스트라고도 한다.

이 절에서는 Python과 scikit-learn을 사용하여 의사결정 포레스트 분류기를 구축하는 방법을 배울 것이다. 또한 다음과 같은 개념도 배울 것이다.

- 부트스트랩 집계(배깅(bagging))란 무엇이며 데이터의 다양한 하위 집합을 만드는 데 어떻게 도움이 되나?
- 배깅을 이용하여 다수의 의사결정 트리를 훈련하는 방법과 트리 구축 과정의 무작위성을 제어하는 방법은 무엇인가?
- 다수결 투표, 가중 투표 또는 평균화와 같은 다양한 방법을 사용하여 여러 의사 결정 트리의 예측을 결합하는 방법은 무엇인가?
- 정확도, 정밀도, 회상도, F1-score 등 다양한 지표를 사용하여 의사결정 포레스트 분류기의 성능을 평가하는 방법은 무엇인가?

부트스트랩 집계가 무엇이고 어떻게 작동하는지 배우는 것으로 시작하겠습니다.

### 부트스트랩 집계(베깅)이란?
부트스트랩 집계, 또는 배깅은 대체로 샘플링하여 데이터의 서로 다른 부분집합을 만들 수 있도록 하는 기술이다. 대체로 샘플링한다(sampling with replcaement)는 것은 부분집합에서 동일한 데이터 포인트를 두 번 이상 선택할 수 있다는 것을 의미한다. 이렇게 하면 각 부분집합마다 데이터 포인트의 조합이 다르며, 일부 데이터 포인트는 부분집합에서 두 번 이상 나타날 수도 있고 아예 나타나지 않을 수도 있다. 각 부분집합의 크기는 보통 원래 데이터 집합과 같지만 더 작기나 더 큰 클 수도 있다.

배깅은 의사결정 포레스트의 의사결정 트리들 사이에 다양성을 만드는 데 유용한데, 각 트리들은 데이터의 서로 다른 부분집합에 대해 훈련될 것이기 때문이다. 이것은 트리들 사이의 상관관계를 줄이고 포레스트를 소음과 과적합에 더 강건하게 만든다. 배깅은 또한 많은 트리들의 예측들의 평균을 내기 때문에, 예측의 분산을 줄이는 데 도움이 된다.

다음은 배깅을 사용하여 데이터의 세 부분 집합을 만드는 방법의 예이다.

```python
# Import numpy for random sampling
import numpy as np

# Define the original data set as a numpy array
data = np.array([[5.1, 3.5, 1.4, 0.2, 1], # Iris setosa
                    [4.9, 3.0, 1.4, 0.2, 1], # Iris setosa
                    [7.0, 3.2, 4.7, 1.4, 0], # Iris versicolor
                    [6.4, 3.2, 4.5, 1.5, 0], # Iris versicolor
                    [6.3, 3.3, 6.0, 2.5, 0], # Iris virginica
                    [5.8, 2.7, 5.1, 1.9, 0]]) # Iris virginica

# Define the number of subsets and the size of each subset
n_subsets = 3
subset_size = len(data)

# Create an empty list to store the subsets
subsets = []

# Loop over the number of subsets
for i in range(n_subsets):
    # Sample the data with replacement and store it as a numpy array
    subset = np.random.choice(len(data), size=subset_size, replace=True)
    subset = data[subset]
    # Append the subset to the list
    subsets.append(subset)

# Print the subsets
for i, subset in enumerate(subsets):
    print(f"Subset {i+1}:")
    print(subset)
```

위의 코드 출력은 다음과 같다.

```
Subset 1:
[[5.8 2.7 5.1 1.9 0. ]
 [5.1 3.5 1.4 0.2 1. ]
 [5.1 3.5 1.4 0.2 1. ]
 [6.4 3.2 4.5 1.5 0. ]
 [6.3 3.3 6.  2.5 0. ]
 [4.9 3.  1.4 0.2 1. ]] 
Subset 2:
[[6.4 3.2 4.5 1.5 0. ]
 [6.3 3.3 6.  2.5 0. ]
 [5.8 2.7 5.1 1.9 0. ]
 [6.4 3.2 4.5 1.5 0. ]
 [5.1 3.5 1.4 0.2 1. ]
 [7.  3.2 4.7 1.4 0. ]]
Subset 3:
[[6.3 3.3 6.  2.5 0. ]
 [5.1 3.5 1.4 0.2 1. ]
 [5.8 2.7 5.1 1.9 0. ]
 [7.  3.2 4.7 1.4 0. ]
 [5.1 3.5 1.4 0.2 1. ]
 [4.9 3.  1.4 0.2 1. ]]
```

보다시피 각 부분집합에는 서로 다른 데이터 포인트의 조합이 있고, 일부 데이터 포인트는 일부 부분집합에서 반복되거나 누락된다. 이것이 바로 배깅이 작동하여 데이터의 서로 다른 부분집합을 만드는 방법이다.

### 배깅을 사용하여 여러 의사 결정 트리를 훈련하는 방법은 무엇인가?
배깅을 사용하여 데이터의 다른 부분집합을 만든 다음, 각 부분집합을 사용하여 여러 의사결정 트리를 훈련할 수 있다. 이렇게 하면 각 트리는 데이터의 다른 부분으로부터 학습하고 다른 패턴과 관계를 포착할 수 있다. 그러나 트리 구축 과정에서 무작위성을 도입하여 트리의 다양성을 더욱 높일 수도 있다. 이는 다음 두 가지 방법을 사용하여 수행할 수 있다.

- **무작위 부분공간 방법(random subspace method)**: 이 방법은 모든 특징을 사용하는 대신 트리의 각 노드에서 특징의 부분집합을 무작위로 선택한다. 이것은 트리들 사이의 상관관계를 줄이고 트리들을 더 독립적이고 다양하게 만든다.
- **무작위 분할점 방법(random split point method)**: 이 방법은 정보 이득을 극대화하는 최적의 분할점을 사용하는 대신 트리의 각 노드에서 각 특징에 대한 분할점을 무작위로 선택한다. 이는 트리 구조에 약간의 잡음과 변동성을 도입하고 과적합이 발생하기 쉽게 만든다.

이러한 방법들과 bagging을 결합하면 다양하고 많은 의사결정 트리들로 구성된 의사결정 포레스트를 만들 수 있다. 포레스트의 트리 수는 분류기의 성능을 최적화하기 위해 조정할 수 있는 하이퍼파라미터이다. 일반적으로 트리가 많을수록 정확도가 향상되지만 계산 비용과 과적합 위험도 높아진다.

다음은 Python과 scikit-learn을 이용한 bagging과 랜덤 부분공간 방법을 이용하여 여러 의사결정 트리를 훈련하는 예이다.

```python
# Import the required libraries
import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import BaggingClassifier

# Load the data set as a pandas dataframe
data = pd.read_csv("iris_modified.csv")

# Separate the features and the label
X = data.drop("setosa", axis=1)
y = data["setosa"]

# Define the number of trees and the number of features to use at each node
n_trees = 10
n_features = 2

# Create a decision tree classifier with random state for reproducibility
tree = DecisionTreeClassifier(random_state=42)

# Create a bagging classifier with random state and random subspace method
forest = BaggingClassifier(base_estimator=tree, n_estimators=n_trees, max_features=n_features, bootstrap=True, random_state=42)

# Train the forest classifier on the data
forest.fit(X, y)
```

이 코드는 각 노드에서 무작위로 선택된 2개의 특징을 사용하여 각각 10개의 트리로 의사결정 포레스트를 생성할 것이다. DecisionTreeClassifier의 스플리터 파라미터를 "random"으로 설정하여 랜덤 분할점 방식을 사용할 수도 있다.

### 다중 의사결정 트리의 예측을 결합하는 방법?
배깅과 랜덤 부분공간 또는 분할점 방법을 사용하여 여러 의사결정 트리를 훈련한 후에는 의사결정 포레스트 분류기에 대한 최종 예측을 수행하기 위해 여러 의사결정 트리의 예측을 결합해야 한다. 여러 의사결정 트리의 예측을 결합하는 방법은 문제의 유형과 트리의 출력에 따라 다양하다. 일반적인 방법은 다음과 같다.

- **다수결 투표(major voting)**: 이 방법은 각 트리가 클래스 레이블을 출력하는 분류 문제에 사용된다. 최종 예측은 트리로부터 가장 많은 표를 받는 클래스 레이블이다. 예를 들어, 10개의 트리가 있고 그 중 6개가 클래스 A를 예측하고 그 중 4개가 클래스 B를 예측하면 최종 예측은 클래스 A가 된다.
- **가중치 투표(weighted voting)**: 이 방법은 분류 문제에도 사용되지만 정확도나 신뢰도와 같은 몇 가지 기준에 따라 트리의 투표에 다른 가중치를 부여한다. 최종 예측은 트리의 투표에 대한 가중치 합이 가장 높은 클래스 레이블이다. 예를 들어, 10개의 트리가 있고 그 중 6개가 클래스 A와 4개를 예측하지만 클래스 A를 예측하는 트리는 클래스 B를 예측하는 트리보다 가중치가 더 높다면 가중치가 동일하지 않더라도 최종 예측은 여전히 클래스 A일 수 있다.
- **평균화(averaging)**: 이 방법은 각 트리가 수치를 출력하는 회귀 문제에 사용된다. 최종 예측은 트리가 출력한 값의 평균이다. 예를 들어, 10개의 트리가 있고 트리가 1, 2, 3, 4, 5, 6, 7, 8, 9, 10을 출력하면 최종 예측은 5.5이다.

이러한 방법들은 다수의 의사결정 트리들의 예측들을 조합하고 의사결정 포레스트 분류기의 정확성과 강건성을 향상시키는 간단하고 효과적인 방법들이다. 문제와 데이터에 따라 중위수, 모드 또는 가중 평균과 같은 다른 방법들을 사용할 수도 있다.

### Python과 scikit-learn을 사용하여 의사 결정 포레스트 분류기를 구현하는 방법은 무엇인가?
이 절에서는 Python과 scikit-learn을 사용하여 의사결정 포레스트 분류기를 구현하는 방법을 배울 것이다. Scikit-learn은 Python에서 머신러닝을 위한 인기 있고 강력한 라이브러리로 데이터 분석과 모델링을 위한 많은 도구와 알고리즘을 제공한다. 다음 명령을 사용하여 scikit-learn을 설치할 수 있다.

```python
# Install scikit-learn
pip install scikit-learn
```

Scikit-learn에는 RandomForestClassifier라는 의사결정 포레스트 분류기를 위한 클래스가 내장되어 있다. 이 클래스를 사용하면 다양한 매개변수와 옵션으로 의사결정포레스트 분류기를 생성하고 훈련할 수 있다. [앞 절](#sec_04)에서와 같이 DecisionTreeClassifier 클래스를 기본 추정기로 사용하여 BaggingClassifier 클래스를 사용할 수도 있지만, RandomForestClassifier 클래스는 다음과 같은 장점이 있다.

- 랜덤 부분공간법과 랜덤 분할점 방법을 이용하여 다양한 트리를 자동으로 생성한다
- 예측을 위한 각 특징의 상대적 중요도를 반환하는 `feature_importances_` 속성을 가진다.
- 각 트리의 학습에 사용되지 않은 샘플을 바탕으로 포레스트의 일반화 오차를 추정한 아웃백 스코어를 반환하는 `oob_score_` 속성을 가진다.

다음은 RandomForestClassifier 클래스를 사용하여 이전 섹션에서 사용한 것과 동일한 데이터 세트에 대한 의사 결정 포레스트 분류기를 만들고 교육하는 방법의 예이다.

```python
# Import the required libraries
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

# Load the data set as a pandas dataframe
data = pd.read_csv("iris_modified.csv")

# Separate the features and the label
X = data.drop("setosa", axis=1)
y = data["setosa"]

# Define the number of trees and the number of features to use at each node
n_trees = 10
n_features = 2

# Create a random forest classifier with random state for reproducibility
forest = RandomForestClassifier(n_estimators=n_trees, max_features=n_features, bootstrap=True, random_state=42)

# Train the forest classifier on the data
forest.fit(X, y)
```

이 코드는 각 노드에서 무작위로 선택된 2개의 특징을 사용하여 10개의 트리로 의사결정 포레스트를 생성할 것이다. 임의 분할점 방식을 사용하고자 한다면 DecisionTreeClassifier의 분할기 파라미터를 사용하여 `"random"`으로 설정할 수도 있다.

### 의사결정 포레스트 분류기의 성능을 평가하는 방법?
의사 결정 포리스트 분류기의 성능을 평가하려면 다음과 같은 다양한 메트릭과 방법을 사용할 수 있다.

- **정확도(accuracy)**: 이는 전체 예측 수에서 올바른 예측이 차지하는 비율이다. 분류기가 데이터에 대해 얼마나 잘 수행하는지 측정하는 것은 간단하고 직관적인 방법이다. 그러나 데이터가 불균형하다면 오해의 소지가 있을 수 있으며, 이는 일부 클래스가 다른 클래스보다 더 자주 수행된다는 것을 의미한다. 예를 들어, 샘플의 90%가 클래스 A에 속하고 10%가 클래스 B에 속하며, 분류기가 항상 클래스 A를 예측하는 데이터 세트가 있다면, 90%의 정확도를 가질 것이지만 좋은 분류기는 아닐 것이다.
- **정밀도(precision)과 리콜(recall)**: 이것들은 분류기가 각 클래스에 대해 관련 샘플을 얼마나 잘 식별하는지 측정하는 두 가지 지표이다. 정밀도는 전체 긍정 예측 수(클래스에 속하는 것으로 예측되는 샘플) 중 진정한 긍정(클래스에 속하는 것으로 정확하게 예측되는 샘플)의 비율이다. 리콜은 전체 실제 긍정(실제 클래스에 속하는 것으로 예측되는 샘플) 중 진정한 긍정의 비율이다. 이러한 지표는 각 클래스에 대한 분류기의 성능을 개별적으로 평가하고 불균형한 데이터를 처리하는 데 유용하다. 그러나 하나를 개선하면 다른 하나를 줄일 수 있다는 것을 의미하며 서로 상충될 수 있다.
- **F1-score**: 고조파 평균(harmonic mean)을 사용하여 정밀도와 리콜을 하나의 값으로 결합한 메트릭이다. 0에서 1까지 다양하며, 여기서 1이 가장 우수하고 0이 가장 열악한 것이다. 정밀도와 리콜의 균형을 맞추고 서로 다른 분류기의 성능을 비교하는 방법이다.
- **혼동 행렬(confusion matric)**: 이는 각 클래스에 대한 참 긍정, 거짓 긍정, 참 부정, 거짓 부정의 수를 보여주는 표이다. 분류기가 각 클래스에 대해 어떻게 수행하고 어떤 클래스를 다른 클래스와 어떻게 혼동하는지 시각적으로 확인할 수 있는 방법이다.
- **교차 검증(cross validation)**: 이것은 데이터를 k개의 접힘으로 분할하는 방법이며, 여기서 k는 당신이 선택한 숫자이다. 그런 다음, 그것은 k-1 접힘에서 분류기를 훈련시키고 나머지 접힘에서 그것을 테스트한다. 그것은 이 과정을 k번 반복하고, 매번 다른 접힘을 테스트하기 위해 사용한다. 그런 다음 분류기의 성능에 대한 추정치를 얻기 위해 k번의 테스트 결과의 평균을 낸다. 이 방법은 성능 추정치의 분산을 줄이고 과적합을 피하기 위해 유용하다.

Scikit-learn은 이러한 메트릭과 메소드를 계산하고 표시하기 위한 함수와 클래스를 제공한다. 앞 절에서 만든 의사결정 포레스트 분류기를 평가하는 데 이를 사용하는 방법의 예는 다음과 같다.

```python
# Import the required libraries
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix, classification_report
from sklearn.model_selection import cross_val_score

# Load the data set as a pandas dataframe
data = pd.read_csv("iris_modified.csv")

# Separate the features and the label
X = data.drop("setosa", axis=1)
y = data["setosa"]

# Define the number of trees and the number of features to use at each node
n_trees = 10
n_features = 2

# Create a random forest classifier with random state for reproducibility
forest = RandomForestClassifier(n_estimators=n_trees, max_features=n_features, bootstrap=True, random_state=42)

# Train the forest classifier on the data
forest.fit(X, y)

# Make predictions on the data
y_pred = forest.predict(X)

# Calculate and print the accuracy
accuracy = accuracy_score(y, y_pred)
print(f"Accuracy: {accuracy}")

# Calculate and print the precision, recall, and f1-score for each class
precision = precision_score(y, y_pred, average=None)
recall = recall_score(y, y_pred, average=None)
f1 = f1_score(y, y_pred, average=None)
print(f"Precision: {precision}")
print(f"Recall: {recall}")
print(f"F1-score: {f1}")

# Print a classification report that summarizes the precision, recall, and f1-score for each class
report = classification_report(y, y_pred)
print(report)

# Print a confusion matrix that shows the number of true positives, false positives, true negatives, and false negatives for each class
matrix = confusion_matrix(y, y_pred)
print(matrix)

# Perform a 5-fold cross-validation and print the mean and standard deviation of the accuracy
scores = cross_val_score(forest, X, y, cv=5)
mean = np.mean(scores)
std = np.std(scores)
print(f"Cross-validation accuracy: {mean} +/- {std}")
```

위의 코드 출력은 다음과 같다.

```
Accuracy: 0.9866666666666667
Precision: [1.         0.97368421]
Recall: [0.97368421 1.        ]
F1-score: [0.98666667 0.98666667]
                precision    recall  f1-score   support

            0       1.00      0.97      0.99        38
            1       0.97      1.00      0.99        37

accuracy                           0.99        75
macro avg       0.99      0.99      0.99        75
weighted avg       0.99      0.99      0.99        75

[[37  1]
 [ 0 37]]
Cross-validation accuracy: 0.9733333333333334 +/- 0.024944382578492935
```

보다시피 의사결정 포레스트 분류기는 두 클래스 모두 정확도가 높고 정밀도, 회상도, f1-score가 높다. 혼동 행렬은 클래스 0의 한 표본만을 클래스 1로 잘못 분류한 것을 보여준다. 교차 검증 정확도 역시 분류기가 데이터의 서로 다른 분할에 걸쳐 일관되고 안정적임을 보인다.

## <a name="sec_06"></a> 의사결정 트리와 포레스트의 장단점
본 절에서는 의사결정 트리와 포레스트의 주요 장단점을 정리하고, 다른 분류 기법과 비교한다.

### 의사결정 트리와 포레스트의 장점
의사결정 트리와 포레스트의 장점은 다음과 같다.

- 예측을 하기 위한 명확하고 논리적인 규칙 집합을 제공하기 때문에 이해하고 해석하기 쉽다. 그래프로 시각화할 수도 있어 분류기 뒤에 숨겨진 추론을 설명하는 데 도움이 될 수 있다.
- 수치적 특징과 범주적 특징을 모두 다룰 수 있으며 데이터의 스케일링이나 정규화를 필요로 하지 않는다. 그들은 또한 결측값에 대해 가장 빈도가 높은 값이나 중앙값을 사용함으로써 결측값과 이상치를 다룰 수 있다.
- 데이터를 분할할 최적의 특징을 선택하고, 특징이 분할에 얼마나 자주 사용되는지를 측정함으로써 특징 선택과 특징 중요도를 수행할 수 있다. 이는 데이터의 차원성과 복잡성을 줄이고, 분류 작업에 가장 관련성이 높은 특징을 식별하는 데 도움이 될 수 있다.
- 특징들 사이의 상호작용들과 의존성들을 포착할 수 있는 다수의 분기들과 분할들을 생성함으로써, 특징들과 클래스 사이의 비선형적이고 복잡한 관계들을 처리할 수 있다.
- 다른 분류기들과 결합하여 의사결정 포레스트을 형성할 수 있으며, 이는 개별 트리들의 잡음과 오류들을 평균화함으로써 분류기의 정확성과 강건성을 향상시킬 수 있다. 이들은 또한 훈련 과정에서 무작위성과 다양성을 도입함으로써 의사결정 트리들의 분산과 과적합을 감소시킬 수 있다.

### 의사결정 트리와 포레스트의 단점
의사결정 트리와 포레스트의 단점은 다음과 같다.

- 특히 트리가 너무 깊거나 너무 복잡할 때 또는 데이터가 노이즈가 많거나 많은 특징을 가질 때 과적합과 높은 분산이 발생하기 쉬울 수 있다. 이는 일반화가 불량하고 데이터의 작은 변화에 대한 높은 민감도를 초래할 수 있다. 과적합을 피하기 위해, 이들은 하이퍼파라미터와 최대 깊이, 최소 샘플, 최소 정보 이득 및 최소 불순물 같은 정지 기준의 세심한 튜닝을 필요로 한다.
- 특히 데이터가 불균형하거나 치우치거나, 또는 특징들이 상이한 스케일 또는 유닛들을 가질 때 편향되고 불안정할 수 있다. 이는 일부 클래스 또는 특징들에 대해 성능이 저하되고 정확도가 저하되는 결과를 초래할 수 있다. 이를 극복하기 위해, 이들은 클래스들의 밸런싱, 특징들의 스케일링 또는 데이터의 변환과 같은 데이터의 전처리를 필요로 한다.
- 특히 트리가 너무 크거나 너무 많은 트리가 사용되는 경우 계산 비용이 많이 들고 메모리 집약적일 수 있다. 이는 트레이닝, 예측 시간 및 저장 공간을 증가시킬 수 있다. 이를 줄이기 위해, 트리의 가지치기 또는 트리의 수를 제한할 필요가 있다.
- 특히 트리가 너무 복잡하거나 너무 많은 트리가 사용되는 경우 검증과 테스트가 어려울 수 있다. 이는 분류기의 성능과 신뢰도를 평가하고 다른 분류기와 비교하는 것을 어렵게 할 수 있다. 이를 해결하기 위해서는 교차 검증, 또는 다른 통계적 방법이 필요하다.

보다시피, 의사결정 트리와 포레스트는 장단점이 있으며 모든 분류 문제에 적합한 것은 아니다. 그러나 여전히 많은 실제 시나리오에 대해 효과적이고 효율적인 솔루션을 제공할 수 있는 강력하고 다재다능한 기법이다. [다음 절](#sec_07)에서는 이러한 응용 사례와 사례 중 일부를 살펴보도록 하겠다.

## <a name="sec_07"></a> 의사결정 트리와 포레스트의 응용과 사례
의사결정 트리와 포레스트는 다음과 같은 다양한 영역과 산업에서 많은 응용과 예를 가지고 있다.

- **의학적 진단(medical dianosis)**: 의사결정 트리와 포레스트는 환자의 증상과 병력을 기반으로 질병과 상태를 진단하는 데 사용될 수 있다. 예를 들어, 의사결정 트리는 환자의 혈당, 혈압, 나이, 체중 등을 기반으로 당뇨병 유무를 진단하는 데 사용될 수 있다.
- **고객 세분화(customer segmentation)**: 고객의 선호도, 행동, 인구 통계 등을 기반으로 의사결정 트리와 포레스트를 사용하여 고객을 서로 다른 그룹으로 세분화할 수 있다. 예를 들어 의사결정 포레스트를 사용하여 고객의 구매 빈도, 최신성, 금액 등을 기반으로 충성도, 잠재력 또는 혼란스러운 고객으로 세분화할 수 있다.
- **사기 탐지(Fraud detection)**: 데이터의 패턴과 이상 징후를 기반으로 사기 거래와 활동을 탐지하기 위해 의사결정 트리와 포레스트를 사용할 수 있다. 예를 들어, 의사결정 포레스트을 사용하여 거래 금액, 위치, 시간, 가맹점 등을 기반으로 신용카드 사기를 탐지할 수 있다.
- **이미지 분류(image classification)**: 의사결정 트리와 포레스트는 이미지의 특징과 픽셀에 기초하여 이미지를 서로 다른 범주로 분류하는 데 사용될 수 있다. 예를 들어, 의사결정 포레스트는 꽃잎과 잎의 모양, 색상, 크기 등에 기초하여 꽃의 이미지를 서로 다른 종으로 분류하는 데 사용될 수 있다.
- **텍스트 분석(text analysis)**: 의사결정 트리와 포레스트는 텍스트를 분석하고 텍스트의 단어와 문장을 기반으로 정보를 추출하는 데 사용될 수 있다. 예를 들어, 의사결정 트리는 리뷰에 있는 긍정적이거나 부정적인 단어와 문구를 기반으로 영화 리뷰의 감정을 분석하는 데 사용될 수 있다.

이들은 의사결정 트리와 포레스트를 응용하고 예시한 것들 중 일부에 불과하지만, 스스로 탐구하고 시도할 수 있는 것들이 더 많다. [마지막 절](#summary)에서는 이 포스팅을 마무리하고 추가 학습을 위한 몇 가지 자료를 제공할 것이다.

## <a name="summary"></a> 마치며
이 포스팅에서는 의사결정 트리와 포레스트를 분류 작업에 사용하는 방법을 배웠다. 의사결정 트리와 포레스트의 기본 개념과 용어, 작동 원리 등을 살펴보았다. Python과 scikit-learn을 사용하여 의사결정 트리 분류기와 의사결정 포레스트 분류기를 구축하고 평가하는 방법도 배웠다. 의사결정 트리와 포레스트의 장단점과 다양한 영역과 산업에서의 적용 사례와 사례도 일부 탐구했다.

의사결정 트리와 포레스트, 또는 다른 분류 기법에 대해 더 알고 싶다면 다음 리소스를 참고할 수 있다.

- [A Gentle Introduction to Decision Trees and Forests](?): 의사결정 트리와 포레스트에 대한 기본을 간단하고 직관적으로 설명하는 블로그 게시물.
- [Decision Trees and Forests in Python](?): scikit-learn을 이용하여 의사결정 트리와 포레스트를 구현하는 방법을 보여주는 동영상 튜토리얼.
- [Classification and Regression Trees](https://www.google.co.kr/books/edition/Classification_and_Regression_Trees/MGlQDwAAQBAJ?hl=ko&gbpv=1&dq=Classification+and+Regression+Trees&printsec=frontcover) : 의사결정 트리와 포레스트의 이론과 응용을 심도 있게 다룬 책.
- [scikit-learn Documentation](https://scikit-learn.org/0.21/documentation.html) : 기계학습을 위한 라이브러리 이용 방법에 대한 자세한 정보와 예시를 제공하는 scikit-learn의 공식 문서이다.
- [Python Data Science Handbook](https://www.google.co.kr/books/edition/Python_Data_Science_Handbook/rimgEAAAQBAJ?hl=ko&gbpv=1&dq=Python+Data+Science+Handbook&printsec=frontcover): Python을 이용한 데이터 사이언스의 기본과 우수 사례를 다룬 책.
