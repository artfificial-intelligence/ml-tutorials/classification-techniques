# 분류 기법: 의사 결정 트리와 포레스트 <sup>[1](#footnote_1)</sup>

> <font size="3">분류 작업에 의사결정 나무와 포레스트를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./classification-techniques.md#intro)
1. [의사결정 나무란?](./classification-techniques.md#sec_02)
1. [의사결정 트리를 구축하는 방법](./classification-techniques.md#sec_03)
1. [의사결정 포레스트란?](./classification-techniques.md#sec_04)
1. [의사결정 포레스트를 구축하는 방법](./classification-techniques.md#sec_05)
1. [의사결정 나무와 포레스트의 장단점](./classification-techniques.md#sec_06)
1. [의사결정 나무와 포레스트의 응용과 사례](./classification-techniques.md#sec_07)
1. [마치며](./classification-techniques.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 4 — Classification Techniques: Decision Trees and Forests](https://medium.com/datadriveninvestor/ml-tutorial-4-classification-techniques-decision-trees-and-forests-cfdbfa07a5b9?sk=cf087127d46bfd7539d9a2b747d4eb04)를 편역하였습니다.
